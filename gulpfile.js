var gulp = require('gulp');
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var consolidate = require('gulp-consolidate');
var runTimestamp = Math.round(Date.now() / 1000);

var fontName = 'icon-font';

// Generate icon fonts
gulp.task('icon', async () => {
  gulp.src(['src/assets/fonts/icons/*.svg'])
    .pipe(iconfontCss({
      fontName: fontName,
      path: 'src/assets/fonts/template/_icons.scss',
      targetPath: '../scss/_icon-font.scss',
      fontPath: '/assets/fonts/',
      centerHorizontally: true
    }))
    .pipe(iconfont({
      fontName: fontName,
      fontHeight: 1001,
      normalize: true,
      timestamp: runTimestamp
    }))

    .on('glyphs', function (glyphs, options) {
      gulp.src('src/assets/fonts/template/iconfont.css')
        .pipe(consolidate('underscore', {
          glyphs: glyphs,
          fontName: options.fontName,
          fontDate: new Date().getTime()
        }))
        .pipe(gulp.dest('src/assets/fonts/CIP-demo-icons'));

      gulp.src('src/assets/fonts/template/index.html')
        .pipe(consolidate('underscore', {
          glyphs: glyphs,
          fontName: options.fontName
        }))
        .pipe(gulp.dest('src/assets/fonts/CIP-demo-icons'));
    })
    .pipe(gulp.dest('src/assets/fonts'));
});