import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadComponent } from './load.component';
import { MapMarkerComponent } from './map-marker/map-marker.component';
import { MenuService } from './services/menu.service';
import { HeaderComponent } from './header/header.component';
import { AgmCoreModule } from '@agm/core';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxAsideModule } from '../../lib/aside/aside.module';

@NgModule({
  declarations: [
    LoadComponent,
    HeaderComponent,
    MapMarkerComponent
  ],
  imports: [
    CommonModule,
    NgxAsideModule,
    TooltipModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAjuWEEcP38PbWwuwt3SK3OMr4HBfPd5jI',
      libraries: ['places']
    })
  ],
  exports: [
    CommonModule,
    NgxAsideModule,
    TooltipModule,
    BsDatepickerModule,
    ModalModule,
    TabsModule,
    LoadComponent,
    HeaderComponent,
    MapMarkerComponent
  ],
  providers: [MenuService]
})
export class SharedModule { }
