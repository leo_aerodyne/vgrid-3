import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map, mergeMap } from 'rxjs/operators';
import { MenuService } from '../services/menu.service';
import { AuthService } from '../../authentication/shared/auth.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SelectProjectComponent } from '../../home/select-project/select-project.component';

@Component({
  selector: 'aerodyne-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  @Input() layout;
  modalRef: BsModalRef;
  config = {
    animated: false
  };

  pageInfo;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    public authService: AuthService,
    public modalService: BsModalService) {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .pipe(map(() => this.activatedRoute))
      .pipe(
        map(route => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        })
      )
      .pipe(filter(route => route.outlet === 'primary'))
      .pipe(mergeMap(route => route.data))
      .subscribe(event => {
        this.titleService.setTitle(event['title']);
        this.pageInfo = event;
      });
  }
  ngOnInit() { }

  // Logout the application
  logout() {
    this.modalRef.hide();
    this.authService.doLogout()
  }

  // Ignore the logout
  decline(): void {
    this.modalRef.hide();
  }

  // Confirm the modal logout
  confirmModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  // Show Modal to select client and Projects
  selectClientsProject(){

    // Adding Class to modal
    this.config['class'] = 'modal-lg modal-dialog-centered model-search';

    // Triggering the modal popup
    this.modalService.show(SelectProjectComponent, this.config);
  }
}
