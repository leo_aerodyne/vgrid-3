import { AgmMap } from '@agm/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import data from './shared/map-data.json';

@Component({
  selector: 'aerodyne-map-marker',
  templateUrl: './map-marker.component.html'
})
export class MapMarkerComponent implements OnInit {
  @ViewChild('map') public map: AgmMap;
  zoom: number = 20;
  lightRustMapStyle: any = data.style;
  liveMarker: any = data.marker;
  markers: [];

  constructor() {}

  ngOnInit(): void {
  }

  onMouseOver(infoWindow, map): void {
    if (map.lastOpen != null) {
      map.lastOpen.close();
    }
    map.lastOpen = infoWindow;
    infoWindow.open();
  }
}
