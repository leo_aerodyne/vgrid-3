import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map, tap  } from 'rxjs/operators';
import {
  HttpClient,
  HttpHeaders,
} from '@angular/common/http';
import { Router } from '@angular/router';
import { HandleErrorService } from '../../shared/services/handle-error.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  endpoint: string = environment.serverURL + '/api';
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  currentUser = {};

  constructor(private http: HttpClient, public router: Router, private handleErrorService: HandleErrorService) {
    console.log(this.endpoint);
  }

  // Sign-up
  // signUp(user: User): Observable<any> {
  //   let api = `${this.endpoint}/register-user`;
  //   return this.http.post(api, user).pipe(catchError(this.handleError));
  // }

  signIn(user): Observable<any> {
    return this.http.post<any[]>(`${this.endpoint}/login`, user).pipe(
      tap(_ => console.log('Login successfull')),
      catchError(this.handleErrorService.handleError)
    );
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('access_token');
    return authToken !== null ? true : false;
  }

  doLogout() {
    let removeToken = localStorage.removeItem('access_token');
    if (removeToken == null) {
      this.router.navigate(['/login']);
    }
  }

}
