import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Common } from '../../shared/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'aerodyne-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  loginFormSubmitted = false;
  isLoginFailed = false;

  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrManager,
    public authService: AuthService,
    public common: Common
  ) {}

  ngOnInit(): void {}

  get lf() {
    return this.loginForm.controls;
  }
  // On submit button click
  onSubmit() {
    this.loginFormSubmitted = true;
    if (this.loginForm.invalid) {
      return;
    }

    this.spinner.show(undefined, {
      type: 'ball-triangle-path',
      size: 'medium',
      bdColor: 'rgba(0, 0, 0, 0.8)',
      color: '#fff',
      fullScreen: true,
    });

    this.authService.signIn(this.loginForm.value).subscribe(
      (result) => {
        this.spinner.hide();
        localStorage.setItem('access_token', result.data.access_token);
        this.router.navigate(['/home']);
      },
      (err: HttpErrorResponse) => {
        this.toastr.errorToastr( err.error && err.error.message ? err.error.message : this.common.ToastInfo.wrong,this.common.ToastInfo.failure);
        this.spinner.hide();
        this.loginFormSubmitted = false;
      }
    );
  }
}
