import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { AuthGuard } from "../shared/services/auth.guard";


const routes: Routes = [
  {
    path: '',
    component: HomeComponent, 
    canActivate: [AuthGuard],
    data: {
      title: 'Home',
      urls: [{ title: 'Home', icon: 'icon icon-home' }]
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }