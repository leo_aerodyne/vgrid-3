import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home.component';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { ProjectCampaignsComponent } from './project-campaigns/project-campaigns.component';
import { CampaignsDefectComponent } from './campaigns-defect/campaigns-defect.component';
import { DefectGalleryComponent } from './defect-gallery/defect-gallery.component';
import { SelectProjectComponent } from './select-project/select-project.component';

@NgModule({
  declarations: [HomeComponent, ProjectCampaignsComponent, CampaignsDefectComponent, SelectProjectComponent, DefectGalleryComponent],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule,
    NgxGalleryModule
  ]
})
export class HomeModule { }
