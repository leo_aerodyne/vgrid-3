import { Component, OnInit } from '@angular/core';
import data from './shared/data.json';

@Component({
  selector: 'aerodyne-search',
  templateUrl: './select-project.component.html',
  styleUrls: ['./select-project.component.scss']
})
export class SelectProjectComponent implements OnInit {

  data: any = data;
  viewMode = 'tab1';
  isProject: boolean = false;
  isClient: boolean = false;

  constructor() { }

  ngOnInit(): void {
    console.log(this.data);
  }

  // Hide Client form and Project list
  // Show project form
  clickOnProject(){
    this.isProject = true;
    this.isClient = false;
  }

  // Hide Project form and Project list
  // Show Client form
  clickOnClient(){
    this.isClient = true;
    this.isProject = false;
  }

  // Hide both Project and Client forms
  // Show project list
  reset(){
    this.isProject = false;
    this.isClient = false;
  }
}
