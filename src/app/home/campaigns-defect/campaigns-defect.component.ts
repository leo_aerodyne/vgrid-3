import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { DefectGalleryComponent } from './../defect-gallery/defect-gallery.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'aerodyne-campaigns-defect',
  templateUrl: './campaigns-defect.component.html',
  animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
  ]
})
export class CampaignsDefectComponent implements OnInit {

  // Animation variabe
  elementRight:string = 'in';

  // Modal popup variable
  modalRef: BsModalRef;
  config = {
    animated: false,
    class: 'modal-xl modal-dialog-centered model-search'
  };

  // Trigger the right sidebar by clicking the icon
  toggleRight(){
    this.elementRight = this.elementRight === 'out' ? 'in' : 'out';
  }

  constructor(public modalService: BsModalService) { }

  ngOnInit(): void {}

  // Trigger the defect modal popup to display the defect-gallery
  // DefectGalleryComponent (Component)
  onClickShowDefectImage(): void {
    this.modalService.show(DefectGalleryComponent, this.config);
  }
}
