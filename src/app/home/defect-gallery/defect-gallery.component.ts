import { Component, OnInit } from '@angular/core';
import {
  NgxGalleryOptions,
  NgxGalleryImage,
  NgxGalleryAnimation
} from '@kolkov/ngx-gallery';

@Component({
  selector: 'aerodyne-defect-gallery',
  templateUrl: './defect-gallery.component.html',
  styleUrls: ['./defect-gallery.component.scss']
})
export class DefectGalleryComponent implements OnInit {
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  constructor() {}

  ngOnInit(): void {

    // Image gallery options/settings
    this.galleryOptions = [
      {
        width: '600px',
        height: '400px',
        thumbnailsColumns: 4,
        arrowPrevIcon: 'icon icon-arrow-left-circle',
        arrowNextIcon: 'icon icon-arrow-right-circle',
        closeIcon: 'icon icon-close',
        fullscreenIcon: 'icon icon-maximize-2',
        spinnerIcon: 'icon icon-rotate-ccw',
        previewFullscreen: true,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];

    // Defect gallery images
    this.galleryImages = this.galleryImages = [
      {
        small:
          'http://www.seawaypainting.com/images/home/seaway-painting-eletric-towers.jpg',
        medium:
          'http://www.seawaypainting.com/images/home/seaway-painting-eletric-towers.jpg',
        big:
          'http://www.seawaypainting.com/images/home/seaway-painting-eletric-towers.jpg'
      },
      {
        small:
          'https://i.cbc.ca/1.4182442.1498693283!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_780/canso-causeway-worker.jpg',
        medium:
          'https://i.cbc.ca/1.4182442.1498693283!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_780/canso-causeway-worker.jpg',
        big:
          'https://i.cbc.ca/1.4182442.1498693283!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_780/canso-causeway-worker.jpg'
      },
      {
        small:
          'https://iconic-talent.com/wp-content/uploads/2017/07/cell-tower-technician-view-climbing-750x410.jpg',
        medium:
          'https://iconic-talent.com/wp-content/uploads/2017/07/cell-tower-technician-view-climbing-750x410.jpg',
        big:
          'https://iconic-talent.com/wp-content/uploads/2017/07/cell-tower-technician-view-climbing-750x410.jpg'
      },
      {
        small:
          'https://upload.wikimedia.org/wikipedia/commons/0/03/NBPowerTransmissionTowerMaintenance.JPG',
        medium:
          'https://upload.wikimedia.org/wikipedia/commons/0/03/NBPowerTransmissionTowerMaintenance.JPG',
        big:
          'https://upload.wikimedia.org/wikipedia/commons/0/03/NBPowerTransmissionTowerMaintenance.JPG'
      }
    ];
  }
}
