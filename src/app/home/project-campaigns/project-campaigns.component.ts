import { Component, OnInit, HostListener } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'aerodyne-project-campaigns',
  templateUrl: './project-campaigns.component.html',
  animations: [
    trigger('slideInOut', [
      state('out', style({
        transform: 'translate3d(-100%, 0, 0)'
      })),
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
  ]
})
export class ProjectCampaignsComponent implements OnInit {
  error: string;
  dragAreaClass: string;
  minDate: any = new Date();
  bsValue: Date = new Date();
  viewMode = 'tab1';
  elementLeft:string = 'out';

  // Trigger the left sidebar by clicking the icon
  toggleLeft(){
    this.elementLeft = this.elementLeft === 'out' ? 'in' : 'out';
  }

  // Get file details when upload or drag and drop
  onFileChange(event: any) {
    let files: FileList = event.target.files;
    this.saveFiles(files);
  }

  ngOnInit() {
    this.dragAreaClass = 'dragarea';
  }

  // Drand and drop images to upload files
  @HostListener('dragover', ['$event']) onDragOver(event: any) {
    this.dragAreaClass = 'droparea';
    event.preventDefault();
  }
  @HostListener('dragenter', ['$event']) onDragEnter(event: any) {
    this.dragAreaClass = 'droparea';
    event.preventDefault();
  }
  @HostListener('dragend', ['$event']) onDragEnd(event: any) {
    this.dragAreaClass = 'dragarea';
    event.preventDefault();
  }
  @HostListener('dragleave', ['$event']) onDragLeave(event: any) {
    this.dragAreaClass = 'dragarea';
    event.preventDefault();
  }
  @HostListener('drop', ['$event']) onDrop(event: any) {
    this.dragAreaClass = 'dragarea';
    event.preventDefault();
    event.stopPropagation();
    if (event.dataTransfer.files) {
      let files: FileList = event.dataTransfer.files;
      this.saveFiles(files);
    }
  }

  // Get the file data
  saveFiles(files: FileList) {
    if (files.length > 1) this.error = 'Only one file at time allow';
    else {
      this.error = '';
      console.log(files[0].size, files[0].name, files[0].type);
    }
  }
}
